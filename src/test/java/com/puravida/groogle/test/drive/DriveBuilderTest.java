package com.puravida.groogle.test.drive;

import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.puravida.groogle.DriveService;
import com.puravida.groogle.DriveServiceBuilder;
import com.puravida.groogle.Groogle;
import com.puravida.groogle.GroogleBuilder;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DriveBuilderTest {

    Groogle groogle;
    DriveService drive;

    @Before
    public void setUp() {
        //tag::register[]
        groogle = GroogleBuilder.build( groogle -> {
            groogle
                    .withOAuthCredentials(credentials ->
                            credentials.storeCredentials(true)
                                    .applicationName("test")
                                    .withScopes(DriveScopes.DRIVE)
                                    .usingCredentials("src/test/resources/client_secret.json")
                    )
                    .register(DriveServiceBuilder.build(), DriveService.class);
        });
        drive = groogle.service(DriveService.class);
        //end::register[]
        assert drive != null;
    }

    @Test
    public void build(){
        assert groogle.service(DriveService.class) != null;
    }

    @Test
    public void allFiles(){
        drive.allFiles( (withFile -> {
            File file = withFile.getFile();
            System.out.println(
                    String.join(" ", file.getId(), "=", file.getName(), "mimeType", file.getMimeType())
            );
        }));
    }

    @Test
    public void findFile(){

        drive.findFiles( filter->

            filter
                    .nameStartWith("Test")
                    .orderBy("name")
                    .batchSize(20)

        , withFile -> {
                    File file = withFile.getFile();
                    System.out.println(
                            String.join(" ", file.getId(), "=", file.getName(), "mimeType", file.getMimeType())
                    );
                });

    }

    @Test
    public void saveFile(){

        drive
                .upload( createFile->
                    createFile
                            .fromContent("Hola caracola")
                            .withName("remove.me")
                )
                .download( saveAs ->
                        saveAs
                                .to("build/"+saveAs.getFile().getName())
                                .asPlainText()
                )
                .removeFromDrive();


    }

    @Test
    public void permissionsFile(){

        DriveService.WithFile withFile =drive
                .upload( createFile->
                    createFile
                            .fromContent("Hola caracola")
                            .withName("permissions.txt")
                );

        synchronized (this){try{wait(5000);}catch (Exception e){}}


        withFile.permissions(permissions ->
                    permissions
                        .usersAsWriter("jorge.aguilera@puravida-software.com")
                        .anyoneAsReader()
                        .strick(true)
                )
            .removeFromDrive();
    }


    @Test
    public void createFolder(){
        String folderName = new SimpleDateFormat("yyyyMMDDHHmmss").format(new Date());
        DriveService.WithFolder withFolder = drive.createFolder(folderName);

        assert  withFolder != null;

        String folderId = withFolder.getId();
        assert folderId != null;

        withFolder.removeFromDrive();
    }

    @Test
    public void uploadFolder(){

        String folderName = new SimpleDateFormat("yyyyMMDDHHmmss").format(new Date());
        DriveService.WithFolder withFolder = drive
                .createFolder(folderName)
                .uploadFrom("src/test/upload");

        final StringBuffer fileId = new StringBuffer();
        drive.findFiles(findFiles ->
            findFiles.
                    parentId(withFolder.getId())
        , withFile -> {
            fileId.append(withFile.getId());
        });

        withFolder
                .removeFromDrive();

        assert fileId.toString().length() != 0;
    }

    @Test
    public void downloadFolder(){

        String folderName = new SimpleDateFormat("yyyyMMDDHHmmss").format(new Date());
        java.io.File folder = new java.io.File("build/"+folderName);
        folder.mkdirs();

        DriveService.WithFolder withFolder = drive
                .createFolder(folderName)
                .uploadFrom("src/test/upload");

        withFolder
                .downloadTo("build/"+folderName)
                .removeFromDrive();

        assert new java.io.File("build/"+folderName+"/test.docx").exists();
        assert new java.io.File("build/"+folderName+"/subfolder/othertest.docx").exists();

        folder.delete();
    }

    @Test
    public void downloadWithFolder(){

        String folderName = new SimpleDateFormat("yyyyMMDDHHmmss").format(new Date());
        java.io.File folder = new java.io.File("build/"+folderName);
        folder.mkdirs();

        DriveService.WithFolder withFolder = drive
                .createFolder(folderName)
                .uploadFrom("src/test/upload");

        drive.withFolder( withFolder.getId(), f -> {
            f.downloadTo("build/" + folderName)
                    .removeFromDrive();
        });

        assert new java.io.File("build/"+folderName+"/test.docx").exists();
        assert new java.io.File("build/"+folderName+"/subfolder/othertest.docx").exists();

        folder.delete();
    }

}
