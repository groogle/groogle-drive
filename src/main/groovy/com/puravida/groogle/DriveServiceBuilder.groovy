package com.puravida.groogle

import com.puravida.groogle.impl.GroovyDriveService
import groovy.transform.CompileStatic

@CompileStatic
class DriveServiceBuilder {

    static DriveService build(){
        new GroovyDriveService()
    }
}
