package com.puravida.groogle.test.drive

import com.google.api.services.drive.DriveScopes
import com.puravida.groogle.DriveService
import com.puravida.groogle.DriveServiceBuilder
import com.puravida.groogle.Groogle
import com.puravida.groogle.GroogleBuilder
import spock.lang.Shared
import spock.lang.Specification

class GroovyDriveBuilderSpec extends Specification{

    @Shared
    Groogle groogle

    void setup(){
        //tag::register[]
        groogle = GroogleBuilder.build {
            withOAuthCredentials {
                applicationName 'test'
                withScopes DriveScopes.DRIVE
                usingCredentials "src/test/resources/client_secret.json"
                storeCredentials true
            }
            service(DriveServiceBuilder.build(), DriveService)
        }
        //end::register[]
    }

    def "service registered"(){
        given: "a drive"

        DriveService service = groogle.service(DriveService)

        expect:
        service
    }

    def "save text file"(){
        given: "a drive"

        DriveService service = groogle.service(DriveService)
        String now = "${new Date()}"
        java.io.File uploadFile = new java.io.File("build/${now}.txt")
        uploadFile.text = "Hi $now"

        service.upload {
            fromFile uploadFile
            intoFolder 'root'
            withName 'example.txt'
        }

        and:
        String fileId
        int count = service.findFiles {
            nameStartWith 'example.txt'
            eachFile {
                fileId = file.id
            }
        }

        when:
        boolean founded = false
        //tag::saveText[]
        service.withFile fileId, {
            println file.name
            println "-----"
            println text
            founded = true
        }
        //end::saveText[]
        then: "exists"
        founded
    }

    def "create text file"(){
        given: "a drive"

        DriveService service = groogle.service(DriveService)

        and:
        String now = "${new Date()}"

        when:
        //tag::createText[]
        DriveService.WithFile fileId = service.upload {
            withName "${now}.txt"
            fromContent "Hi $now"
        }
        //end::createText[]
        then: "exists"
        fileId != null

        and: "can remove it from drive"
        fileId.removeFromDrive()
    }

    def "upload a file"(){
        given: "a drive"

        DriveService service = groogle.service(DriveService)

        when:
        //tag::uploadFile[]
        String now = "${new Date()}"
        java.io.File uploadFile = new java.io.File("build/${now}.txt")
        uploadFile.text = "Hi $now"

        DriveService.WithFile fileId = service.upload(uploadFile)
        //end::uploadFile[]

        then: "exists"
        service.findFile fileId.id

        and: "can remove it from drive"
        fileId.removeFromDrive()
    }

    def "upload a file 2"(){
        given: "a drive"

        DriveService service = groogle.service(DriveService)

        when:
        //tag::uploadFile2[]
        String now = "${new Date()}"
        java.io.File uploadFile = new java.io.File("build/${now}.txt")
        uploadFile.text = "Hi $now"

        DriveService.WithFile fileId = service.upload {
            fromFile uploadFile
            intoFolder 'root'
            withName 'remove-me.txt'
        }
        //end::uploadFile2[]

        then: "exists"
        service.findFile fileId.id

        and: "can remove it from drive"
        fileId.removeFromDrive()

        and: "can remove from local"
        uploadFile.delete()
    }


    def "find files start with test"(){
        given: "a drive"

        DriveService service = groogle.service(DriveService)

        when: "find files"
        String now = "${new Date()}"
        java.io.File uploadFile = new java.io.File("build/${now}.txt")
        uploadFile.text = "Hi $now"

        DriveService.WithFile fileId = service.upload {
            fromFile uploadFile
            intoFolder 'root'
            withName 'me.txt'
        }

        //tag::findFiles[]
        int count = service.findFiles {
            nameStartWith 'me'
            eachFile {
                println file.parents
            }
        }
        //end::findFiles[]
        then: "exists"
        count
    }

    def "download binary file"(){
        given: "a drive"

        DriveService service = groogle.service(DriveService)

        and:
        DriveService.WithFile fileId
        java.io.File uploadFile = new java.io.File("build/example.pdf")
        uploadFile.text = "Hi "

        fileId = service.upload {
            fromFile uploadFile
            intoFolder 'root'
            withName 'example.pdf'
        }

        when:
        //tag::downloadBinary[]
        boolean founded = false
        service.withFile fileId.id, {
            download{
                to "/tmp/example.pdf"
            }
            founded = true
        }
        //end::downloadBinary[]
        then: "exists"
        founded

        and: "can remove it from drive"
        fileId.removeFromDrive()
    }

    def "permissions"(){
        given: "a drive"
        DriveService service = groogle.service(DriveService)

        and: "a file"
        DriveService.WithFile withFile = service.upload {
            fromContent 'hola caracola'
            withName 'permissions.txt'
        }

        sleep 5000

        when: "change permissions"
        withFile.permissions {
            usersAsWriter("jorge.aguilera@puravida-software.com")
            anyoneAsReader()
            strick(true)
        }

        then: "can remove the file"
        withFile.removeFromDrive()
    }
}