package com.puravida.groogle.test.drive;

import com.google.api.services.drive.DriveScopes;
import com.puravida.groogle.DriveService;
import com.puravida.groogle.DriveServiceBuilder;
import com.puravida.groogle.Groogle;
import com.puravida.groogle.GroogleBuilder;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class SharedFolderTest {

    Groogle groogle;
    DriveService drive;

    @Before
    public void setUp() {
        //tag::register[]
        groogle = GroogleBuilder.build( groogle -> {
            groogle
                    .withOAuthCredentials(credentials ->
                            credentials
                                    .storeCredentials(true)
                                    .applicationName("drive-shared")
                                    .withScopes(DriveScopes.DRIVE)
                                    .usingCredentials("src/test/resources/client_secret.json")

                    )
                    .register(DriveServiceBuilder.build(), DriveService.class);
        });
        drive = groogle.service(DriveService.class);
        //end::register[]
        assert drive != null;
    }

    @Ignore
    @Test
    public void createAndDelete(){
        //tag::sharedFolder[]
        String name = "removeme";

        String folderId = drive.createSharedFolder(name);
        assert folderId != null;

        drive.upload( createFile->
                        createFile
                                .intoFolder(folderId)
                                .fromContent("Hola caracola")
                                .withName("remove.txt")
                )
                .removeFromDrive();

        drive.deleteSharedFolder(folderId);
        //end::sharedFolder[]
    }


}
