package com.puravida.groogle.test.drive

import com.google.api.services.drive.DriveScopes
import com.puravida.groogle.DriveService
import com.puravida.groogle.DriveServiceBuilder
import com.puravida.groogle.Groogle
import com.puravida.groogle.GroogleBuilder
import spock.lang.Shared
import spock.lang.Specification

class DriveAsServiceSpec extends Specification{

    @Shared
    Groogle groogle

    void setup(){
        //tag::register[]
        groogle = GroogleBuilder.build {
            withServiceCredentials {
                withScopes DriveScopes.DRIVE
                usingCredentials "src/test/resources/service_secret.json"
            }
            register(DriveServiceBuilder.build(), DriveService)
        }
        //end::register[]
    }

    def "service registered"(){
        given: "a drive"

        def service = groogle.service(DriveService)

        when: "find files"
        //tag::findFiles[]
        int count = service.findFiles {
            eachFile {
                println file.name
            }
        }
        //end::findFiles[]
        then: "no error"
        true
    }

}
